# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This library provides utilities to partition reads from a fastq file into
several files containing subgroups of those reads based on similarity.
"""

# import warnings
import logging
from collections import defaultdict
from itertools import chain
from operator import itemgetter
# from pathlib import Path
from matplotlib import pyplot as plt
import seaborn as sns
from numpy import fromiter, linspace
from scipy.stats import gaussian_kde
from scipy import __version__ as sp_version
from networkx import (
    Graph, complement, connected_components, set_node_attributes)
from networkx import __version__ as nx_version
# This may cause issues in some parallel settings
# from mappy import fastx_read
# from .libbamutils import ali2fq, bam2fastq
import parasail
from parasail import __version__ as parasail_version


# To silence annoying matlotlib logs:
logging.getLogger("matplotlib").setLevel(logging.WARNING)
# See https://www.biostars.org/p/73028/
# and https://pypi.org/project/parasail/#substitution-matrices
# DNA_SUBST_MATRIX = """#
#     A   T   G   C   S   W   R   Y   K   M   B   V   H   D   N   U   *
# A   5  -4  -4  -4  -4   1   1  -4  -4   1  -4  -1  -1  -1  -2  -4  -5
# T  -4   5  -4  -4  -4   1  -4   1   1  -4  -1  -4  -1  -1  -2   5  -5
# G  -4  -4   5  -4   1  -4   1  -4   1  -4  -1  -1  -4  -1  -2  -4  -5
# C  -4  -4  -4   5   1  -4  -4   1  -4   1  -1  -1  -1  -4  -2  -4  -5
# S  -4  -4   1   1  -1  -4  -2  -2  -2  -2  -1  -1  -3  -3  -1  -4  -5
# W   1   1  -4  -4  -4  -1  -2  -2  -2  -2  -3  -3  -1  -1  -1   1  -5
# R   1  -4   1  -4  -2  -2  -1  -4  -2  -2  -3  -1  -3  -1  -1  -4  -5
# Y  -4   1  -4   1  -2  -2  -4  -1  -2  -2  -1  -3  -1  -3  -1   1  -5
# K  -4   1   1  -4  -2  -2  -2  -2  -1  -4  -1  -3  -3  -1  -1   1  -5
# M   1  -4  -4   1  -2  -2  -2  -2  -4  -1  -3  -1  -1  -3  -1  -4  -5
# B  -4  -1  -1  -1  -1  -3  -3  -1  -1  -3  -1  -2  -2  -2  -1  -1  -5
# V  -1  -4  -1  -1  -1  -3  -1  -3  -3  -1  -2  -1  -2  -2  -1  -4  -5
# H  -1  -1  -4  -1  -3  -1  -3  -1  -3  -1  -2  -2  -1  -2  -1  -1  -5
# D  -1  -1  -1  -4  -3  -1  -1  -3  -1  -3  -2  -2  -2  -1  -1  -1  -5
# N  -2  -2  -2  -2  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -2  -5
# U  -4   5  -4  -4  -4   1  -4   1   1  -4  -1  -4  -1  -1  -2   5  -5
# *  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5  -5
# """
#
#
# def make_dna_subst_matrix(fname):
#     """
#     Get DNA substitution matrix from file *fname*, creating it if necessary.
#     """
#     if not Path(fname).exists():
#         with open(fname, "w") as fh:
#             fh.write(DNA_SUBST_MATRIX)
#     return parasail.Matrix(fname)



# def formatwarning(message, category, filename, lineno, line):
#     """Used to format warning messages."""
#     return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


# warnings.formatwarning = formatwarning


try:
    fastx_read
except NameError:
    def fastx_read(fname):
        """Generate fasta records from file *fname*."""
        with open(fname) as fa_in:
            for line in fa_in:
                name = line.strip().split()[0][1:]
                seq = fa_in.readline().strip()
                yield (name, seq)


class Deduplicator():
    """
    Class that can be iterated to retrieve sequences from a fasta file, without
    duplicates, and stores a record of the names under which sequences were
    found.
    """
    __slots__ = ["_seq_gen", "seq_names"]

    def __init__(self, in_fname):
        """
        *in_fname* should be the path to a fasta file
        with sequences on only one line.
        """
        self._seq_gen = fastx_read(str(in_fname))
        # keys: sequences
        # values: lists of sequence names
        self.seq_names = defaultdict(list)

    def __iter__(self):
        """Generate fasta records without duplicate sequences."""
        for (name, seq, *_) in self._seq_gen:
            if not self.seq_names[seq]:
                yield seq
            self.seq_names[seq].append(name)


# TODO, maybe: Use the alignment info to compute consensus step by step
# with intersection when non empty, union otherwise.
# Even better use qualities to resolve mismatches.
def compute_alignment_score(seq1, seq2, matrix, gap_open=5, gap_extend=2):
    """
    Compute an alignement score between sequences *seq1* and *seq2*
    using a nucleotide substitution matrix.
    """
    alignment = parasail.nw_trace(
        seq1, seq2,
        gap_open, gap_extend, matrix)
    return alignment.score
    # See also:
    # https://github.com/jvkersch/scikit-bio/blob/29dcb7448283bf5b55994ebeb38cc375a15226b9/skbio/alignment/parasail.py#L325


def build_similarity_graph(in_fname):
    """
    Generate the graph of sequence similarity for sequences in fasta file
    *in_fname* using a nucleotide substitution matrix.

    Also return a complete graph of sequences, ready for partitioning, and
    scores as a numpy array.

    The nodes of the graphs will be sequences represented as (name, sequence)
    pairs.
    """
    score_graph = Graph()
    seq_generator = Deduplicator(in_fname)
    # Start with node for all sequence, but no edges
    score_graph.add_nodes_from(seq_generator)
    # Version with edges everywhere
    complete_graph = complement(score_graph)
    # Add the sequence names as attributes to the nodes
    set_node_attributes(complete_graph, seq_generator.seq_names, "names")
    # Compute scores for all possible pairs of sequences
    matrix = parasail.nuc44
    for (seq1, seq2) in complete_graph.edges():
        score = compute_alignment_score(seq1, seq2, matrix)
        # Each node corresponds to a certain number of sequences.
        # This in turn makes each edge correspond to
        # <the product of these numbers> eges.
        num_pairs = (
            len(complete_graph.nodes[seq1]["names"])
            * len(complete_graph.nodes[seq2]["names"]))
        score_graph.add_edge(
            seq1, seq2,
            weight=(score, num_pairs))
    return (score_graph, complete_graph)


def get_scores_from_graph(score_graph):
    """
    Extract scores from compacted graph *score_graph*, where edges contain
    "weight" data in the form of (score, num_pairs) pairs.
    """
    # Collect the scores to find a minimum in their distribution
    # The idea is that if we have a genuine mix of molecules,
    # we should have a clear distinction between high scores
    # (between sequences representing the same molecule) and low scores
    # (between sequences representing different molecules).
    return fromiter(
        chain.from_iterable(
            [score] * num_pairs for (score, num_pairs) in map(
                # to extract the weight from (n1, n2, weight) triplets
                itemgetter(2),
                score_graph.edges.data("weight"))),
        dtype=float)


def plot_score_density(scores, min_density_score, axis):
    """
    Plot density distribution of *scores* on axis *axis*, with a vertical line
    at *min_density_score*, the score at which the estimated density is lowest.
    """
    # kde=True plots an estimate of the density
    # (maybe not the same one as obtained using scipy.stats.gaussian_kde)
    # rug=True plots small lines where a value exits
    # his=False disables default plotting of a histogram
    try:
        if len(scores) < 100:
            sns.distplot(scores, kde=True, rug=True, hist=False, ax=axis)
        else:
            # Figure too heavy when lots of rug lines
            sns.distplot(scores, kde=True, rug=False, hist=False, ax=axis)
    except RuntimeError as err:
        if str(err) == "Selected KDE bandwidth is 0. Cannot estiamte density.":
            logging.warn("%s\n" % str(err))
            sns.distplot(scores, kde=False, rug=False, hist=True, ax=axis)
        else:
            raise
    # Vertical bar at min_density_score,
    # indicating the estimated score of minimal density
    # which will be used as threshold to cut edges in the graph.
    axis.axvline(x=min_density_score, color="red")
    axis.text(
        # label right of the line, just below the top of the graph
        x=min_density_score * 1.025, y=axis.get_ylim()[1] * 0.975,
        s=f"{min_density_score}", color="red",
        horizontalalignment="left",
        verticalalignment="top")


def make_sequence_similarity_groups(in_fname, cell_dir):
    """
    Create a graph where sequences from file *in_fname* are connected
    when they are "similar enough".
    Sequence similarity distributions will be plotted in directory *cell_dir*.
    """
    (score_graph, complete_graph) = build_similarity_graph(in_fname)
    if len(score_graph.nodes()) <= 2:
        logging.info(
            "Not enough sequences to make interesting groups for %s",
            in_fname)
        # TODO: Use percent similarity between the 2 sequences
        # to decide whether to split or not.
        return complete_graph
    ####################################################
    # Estimating the density of the score distribution #
    ####################################################
    scores = get_scores_from_graph(score_graph)
    min_score = min(scores)
    max_score = max(scores)
    score_range = max_score - min_score
    score_density = gaussian_kde(scores).evaluate(
        linspace(min_score, max_score, 100))
    # min_density_idx = score_density.argmin()
    # Where is the density lowest? (excluding first and last)
    min_density_idx = score_density[1:-1].argmin()
    # Converting index in the `score_density` array into score value
    # Hoping this is a safe threshold to cut edges in the graph:
    min_density_score = min_score + \
        min_density_idx * score_range / 100
    logging.debug(
        "min_score: %s, cut: %s, max_score: %s",
        min_score, min_density_score, max_score)
    #############################################################
    # Making graphical representation of the score distribution #
    #############################################################
    out_plot = cell_dir.joinpath("sequence_similarity_distribution.pdf")
    fig, axis = plt.subplots()
    plot_score_density(scores, min_density_score, axis)
    plt.savefig(out_plot)
    plt.close(fig)
    logging.info("Similarity score distribution plot: see %s", out_plot)
    #######################
    # Splitting the graph #
    #######################
    split_graph = complete_graph.edge_subgraph(
        (node1, node2)
        for (node1, node2, data) in score_graph.edges.data()
        if data["weight"][0] > min_density_score)
    # TODO
    #############
    # Recursing #
    #############
    # for (set_num, node_set) in enumerate(
    #     connected_components(split_graph), start=1):
    return split_graph


def write_fasta(records, fname):
    """
    Write fasta records *records* in file *fname*.
    *records* should be an iterable of pairs (seq_id, seq).
    """
    with open(fname, "w") as fasta_out:
        for (name, seq) in sorted(records):
            fasta_out.write(f">{name}\n{seq}\n")


def split_fasta(in_fname, cell_id, cell_dir):
    """
    Split fasta file *in_fname* based on nucleotide sequence similarities.

    The resulting fasta files are written in directory *cell_dir*

    Return a list of character strings, each one representing
    a tab-separated line where the first column is the name of a written
    fasta file and the second column the corresponding number of sequences.
    """
    # cell_id = in_fname.name[:-len("_merged.fasta")]
    logging.debug("%s", cell_id)
    # cell_dir = out_dir.joinpath(cell_id)
    logging.info("split_fasta: Creating %s", cell_dir)
    cell_dir.mkdir(parents=True, exist_ok=True)
    split_graph = make_sequence_similarity_groups(in_fname, cell_dir)
    logging.info("Made sequence similarity groups for %s", cell_id)

    def compute_seq_num(node_set):
        """
        Compute the total number of sequences that the nodes in *node_set*
        represent.
        """
        return sum(
            len(split_graph.nodes.data("names")[seq]) for seq in node_set)
    # Paths and number of sequences of the output files
    report_info = []
    # Get connected components
    # node_sets = sorted(
    #     connected_components(split_graph),
    #     key=compute_seq_num, reverse=True)
    node_sets = connected_components(split_graph)
    # Associate size information
    node_sets = [
        (compute_seq_num(node_set), node_set)
        for node_set in node_sets]
    # Sort based on number of sequences (highest first)
    node_sets.sort(reverse=True)
    for (set_num, (seq_num, node_set)) in enumerate(node_sets, start=1):
        fasta_out = cell_dir.joinpath(f"{set_num}.fasta")
        logging.debug("Writing %s", fasta_out)
        # named_sequences = []
        # for seq in node_set:
        #     named_sequences.extend(
        #         (name, seq) for name in split_graph.nodes.data("names")[seq])
        write_fasta(
            chain.from_iterable([
                [(name, seq) for name in split_graph.nodes.data("names")[seq]]
                for seq in node_set]),
            fasta_out)
        report_info.append(f"{fasta_out}\t{seq_num}\n")
    # To be sure this batch has been processed:
    flag_file = cell_dir.joinpath("end.txt")
    with open(flag_file, "w") as fh:
        fh.write(f"# Data for {cell_id} split into {len(node_sets)} sets.\n")
        fh.write("".join(report_info))
        # fh.write(date.today().strftime("%Y-%m-%d-%H:%M:%S\n"))
    return report_info
