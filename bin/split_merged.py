#!/usr/bin/env python3
# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This program partitions reads from a fastq file into several files containing
subgroups of those reads based on similarity.
"""

import sys
major, minor = sys.version_info[:2]
if major < 3 or (major == 3 and minor < 8):
    sys.exit("Need at least python 3.8\n")
import logging
from pathlib import Path
import multiprocessing as mp
import dask
# from mappy import fastx_read
# import parasail
from libclusterseq import split_fasta


# To silence annoying matlotlib logs:
logging.getLogger("matplotlib").setLevel(logging.WARNING)


def main():
    """Main function of the program."""
    logging.basicConfig(level=logging.INFO)
    # logging.basicConfig(level=logging.DEBUG)
    # https://github.com/jeffdaily/parasail/blob/master/parasail/matrices/
    # nuc44.h
    # matrix = parasail.nuc44
    out_dir = Path(sys.argv[2])
    logging.debug("Creating %s", out_dir)
    out_dir.mkdir(parents=True, exist_ok=True)
    in_dir = Path(sys.argv[1])
    in_fnames = in_dir.glob("*_merged.fasta")
    nprocs = int(sys.argv[3]) - 1
    # try:
    #     nprocs = int(environ["SLURM_NTASKS_PER_NODE"]) - 1
    # except KeyError:
    #     nprocs = 1
    # mode = "for loop"
    # mode = "mp"
    mode = "dask"
    if mode == "mp":
        logging.info("Trying to use %s processors (mode %s).", nprocs, mode)
        pool = mp.Pool(processes=nprocs)
        computations = [
            pool.apply_async(
                split_fasta, args=(
                    in_fname,
                    # "Walrus" assignment expression introduced in Python 3.8
                    cell_id := in_fname.name[:-len("_merged.fasta")],
                    out_dir.join_path(cell_id)))
            for in_fname in in_fnames]
        report_list = [comp.get() for comp in computations]
    elif mode == "dask":
        logging.info("Trying to use %s processors (mode %s).", nprocs, mode)
        dask.config.set(scheduler="processes")
        computations = []
        for in_fname in in_fnames:
            cell_id = in_fname.name[:-len("_merged.fasta")]
            computations.append(
                dask.delayed(split_fasta)(in_fname, cell_id, out_dir))
        [report_list] = dask.compute([*computations], num_workers=nprocs)
    else:
        report_list = []
        for in_fname in in_fnames:
            cell_id = in_fname.name[:-len("_merged.fasta")]
            report_list.append(split_fasta(in_fname, cell_id, out_dir))
    logging.info("Finished computations")
    with open(out_dir.joinpath("sequence_numbers.tsv"), "w") as fh:
        for reports in report_list:
            fh.write("".join(reports))
    logging.info("Finished writing reports")
    return 0


sys.exit(main())
