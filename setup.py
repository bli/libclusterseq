# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
#from Cython.Build import cythonize
#from distutils.extension import Extension

name = "libclusterseq"

# Adapted from Biopython
__version__ = "Undefined"
for line in open("%s/__init__.py" % name):
    if (line.startswith('__version__')):
        exec(line.strip())


# https://github.com/cython/cython/blob/master/docs/src/reference/compilation.rst#configuring-the-c-build
#extensions = [
#    Extension(
#        "libreads.libbamutils", ["libreads/libbamutils.pyx"],
#        include_dirs=pysam_get_include()),
#    ]

setup(
    name=name,
    version=__version__,
    description="Miscellaneous things to cluster sequences.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    # >=3.8 because using := in bin/split_merged.py
    # Requirement could be relaxed to >=3.6 otherwise.
    python_requires=">=3.8, <4",
    packages=find_packages(),
    scripts=["bin/split_merged.py"],
    #ext_modules = extensions,
    setup_requires=[
        #"wheel",
        #"cython",
        #"pysam",
    ],
    install_requires=[
        "dask",
        "matplotlib",
        "networkx",
        "numpy",
        #"mappy",
        "parasail",
        "scipy",
        "seaborn",
    ],
)
